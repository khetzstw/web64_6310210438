import BMIResult  from "../components/BMIResult";
import { useState} from "react";
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Container from '@mui/material/Container';



function BMICalPage() {

    const [ name,setName] = useState("");
    const [bmiResult,setBmiResult]= useState(0);
    const [translateResult, setTranslateResult]
                                  = useState("");

    const [height,setHeight] = useState("");
    const [weight,setweight] = useState("");   
    
    function calculateBMI (){
        let h = parseFloat(height);
        let w = parseFloat(weight);
        let bmi = w / (h*h);
        setBmiResult(bmi);
        if(bmiResult>25){
            setTranslateResult("หล่อเท่หล่อเท่หล่อเท่");
        }else{
            setTranslateResult("HandsomeHandsomeHandsome");
        }
    }

    return(
        <Container maxWidth='lg'>
        <Grid container spacing={2} sx={{ marginTop : "10px"}}>
            <Grid item xs={12}>
            <Typography variant="h5">
            ยินดีต้อนรับสู่เว็บคำนวณ BMI : 
            </Typography>
            </Grid>

            <Grid item xs={8}>
                <Box sx={{ textAlign: 'left'}}>
                คุณชื่อ : <input type="text" 
                               value={name}
                               onChange={(e)=>{setName(e.target.value);}}/> 
                               <br />
                               <br />
                สูง : <input type="text" 
                               value={height}
                               onChange={(e)=>{setHeight(e.target.value);}}/> 
                               <br />
                               <br />               
                หนัก : <input type="text" 
                               value={weight}
                               onChange={(e)=>{setweight(e.target.value);}}/> 
                               <br />
                               <br />
                
                
                <Button variant="contained" onClick={ ()=>{calculateBMI()}}>
                   CALCULATE
                </Button>
                </Box>               
            </Grid>
            <Grid item xs={4}>
            {  bmiResult !=0&&
                        <div>
                            <hr />
                            นี่ผลการคำนวณจ้า
                            <BMIResult
                                name={name}
                                bmi={bmiResult} 
                                result={translat eResult}
                                />
                        </div>

                }


            </Grid>
            </Grid>
            </Container>
    );

}

export default BMICalPage; 


/*
        <div align="left">
            <div align="center">
                ยินดีต้อนรับสู่เว็บคำนวณ BMI
                <hr />

                คุณชื่อ : <input type="text" 
                               value={name}
                               onChange={(e)=>{setName(e.target.value);}}/> <br />
                สูง : <input type="text" 
                               value={height}
                               onChange={(e)=>{setHeight(e.target.value);}}/> <br />               
                หนัก : <input type="text" 
                               value={weight}
                               onChange={(e)=>{setweight(e.target.value);}}/> <br />
                
                
                <Button variant="contained" onClick={ ()=>{calculateBMI()}}>
                   CALCULATE
                </Button>
                {  bmiResult !=0&&
                        <div>
                            <hr />
                            นี่ผลการคำนวณจ้า
                            <BMIResult
                                name={name}
                                bmi={bmiResult}
                                result={translateResult}
                                />
                        </div>

                }

        
            </div>
        </div> */