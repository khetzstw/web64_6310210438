import Paper from '@mui/material/Paper';
import Box from '@mui/material/Box';
import { width } from '@mui/system';
function AboutUs (props) {


    return(
        <Box sx={ {width:"60%"}}>
        <Paper elevation={3}>
            <h2>Create By : {props.name}</h2>
            <h3> I AM {props.address} </h3>
            <h3> LIVE IN {props.province} </h3>
        </Paper>
        </Box>
    );
}

export default AboutUs;