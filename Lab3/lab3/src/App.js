import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import Footer from './components/Footer';
import Body from './components/Body';
import Info from './components/Info';
function App() {
  return (
    <div className="App">
      <Header />
      <Info />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
